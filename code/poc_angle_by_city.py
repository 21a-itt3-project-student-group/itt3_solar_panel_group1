from pysolar.solar import *
from geopy.geocoders import Nominatim
import datetime

def get_cords():
	try:
		app = Nominatim(user_agent="tutorial")
		cords = app.geocode("Odense, denmark").raw
		latitude = float(cords['lat'])
		longitude = float(cords['lon'])
		return latitude, longitude 
	except:
		print("could not get cords")

def get_date():
	date = datetime.datetime.now(datetime.timezone.utc)
	return date

def get_angle(latitude, longitude, date):
	sun_angle = get_altitude(latitude, longitude, date)
	return sun_angle

latitude, longitude = get_cords()
print(latitude, longitude)
print(get_angle(latitude, longitude, get_date()))