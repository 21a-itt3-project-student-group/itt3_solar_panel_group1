## Controlling arduino from Raspberry pi

To control the Arduino from a raspberry pi a couple of diffrent options are avaliable as for which protocol to use.
1. I2C
2. SPI
3. usart
4. Serial

<br>

The following program is an example of what could possibly be run on the arduino over serial

void setup() {
   Serial.begin(9600);  // start serial communication at 9600 baud
}
void loop() {
   // Read and execute commands from serial port
   if (Serial.available()) {  // check for incoming serial data
      String command = Serial.readString();  // read command from serial port
      if (command == "pin1_HIGH") {  // pin high
         digitalWrite(pin1, HIGH);
      } else if (command == "pin1_LOW") {  // pin low
         digitalWrite(pin1, LOW);
      }
   }
}


<br>

## Why arduino?:
Arduino allows for 5v output on multiple pins, which in this case means its able to control the actuators without having a relay attatched.



## Programming arduino:
Arduino is programmed in the C++ language, much like the RPI it has GPIO pins allowing for electrical connections.


## brief program idea:
first the pins attatched to the actuators needs to be defined, either 2 or 4 pins 
then the pins has to be set to output and depending on where the solar panel is to be located pins are either set to high or low.
Functionality to power on two pins at a time might be a good idea, as long as they are not in opposite directions.

there might also be some arguments for checking the status of the pins, might be possible with something like this
 pin1State = digitalRead(pin1);
 pin2State = digitalRead(pin2);



<br>

##resources:
Arduino nano pin specs:
https://components101.com/microcontrollers/arduino-nano


Connecting arduino and RPI over I2C protocol:
https://www.deviceplus.com/arduino/connecting-a-raspberry-pi-to-an-arduino-uno/

controlling led with joystick:
https://elekonika.com/analog-joystick-arduino-tutorial/

controlling motor with arduino and joystick.
https://www.engineersgarage.com/how-to-control-dc-motor-speed-direction-using-a-joystick-and-arduino/