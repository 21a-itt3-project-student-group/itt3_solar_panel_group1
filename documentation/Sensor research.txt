Sun sensors are usually classified into three types:

    Analog sensor whose output signal is a continuous function of Sun angle.
    Sun presence sensor which provides a constant output signal when the sun is in field of view.
    Digital sensor that produces encoded discrete output that is measured by the sun angle function.


The sun sensor is operated based on the entry of light into a thin slit on top of a rectangular chamber whose bottom part is lined with a group of light-sensitive cells. The chamber casts an image of a thin line on the chamber bottom. The cells at the bottom measure the distance of the image from a centerline and determine the refraction angle by using the chamber height.

The cells are operated based on the photoelectric effect. They convert the incoming photons into electrons and hence voltages which are in turn converted into a digital signal. When two sensors are placed perpendicular to each other, the direction of the sun with reference to the sensor axes can be calculated.

Basic photoresistors can be used, place 4 of them and split them with four walls, then just follow the one that gets the most light