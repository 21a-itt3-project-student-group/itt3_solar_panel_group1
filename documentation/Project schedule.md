# Project schedule

allocated time for this project is the duration of the 3. semester of the ITT education. 


To keep the project manageble and the group engaged the project will be split into 3 smaller milestones. 

1. Proof of concept (POC)
In essence the POC phase is responsible for validating the developers that the project is doable, both in realtion to avaliable time, currenct market and the skill of  the developers.
the POC phase will include research, documenation, experimental code and hardware setups. These are done for the porpuses of resolving possible obstacels early in the development.
expected output from the POC phase is a product showcasing the feasability of the project and a plan for further developing the product into a MVP.

<br>

2. Minimum viable product (MVP)
The MVP phase is a volatile phase of the project development because the idea has proven feasible and is now ready to grow from the base idea.
This step allows for furhter additions on the project, derrived from the interaction with the costumers, product owners, testers etc.
because of the many possibilities of rapid implementations, in this phase it is vital for the team to keep communicating with eachother aswell as the costumers.
Once the MVP phase is over an actual product should have been developed as well as documented, though it might be lacking case specific capabilities

<br>

3. final adjustments and presentation prep

<br>