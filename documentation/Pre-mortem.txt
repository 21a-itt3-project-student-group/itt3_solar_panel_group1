As a part of the risk assessment process a pre-mortem evaluation is performed. This practice is done to evaluate the potential points of failure in relation to the project. and thus enabling the group to plan ahead avoiding potential failure of the project as a whole.

Points of potential failure:

* Group lack of understanding of concepts relating to planetarian movement
* 