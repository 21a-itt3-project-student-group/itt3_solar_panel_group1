## Basic photoresistor understanding

Photoresistors is made of durable semi-conducter material. 
A photoresistor much like an ordinary resistor works to reduce the ammount of current flowing through a given circuit. 
What differentiates a photoresistor from other resistors is the abillity to change the internal resistance of the component depending on the light exposure.


## implementation of photoresistor in a circut

Looking at a photoresistor from a programming perspective, raises the issue of the photoresistor output being an analog signal. Thus to effectivly be able to use the changes in the circuit resistance as a valid output an analog to digital IC/chip has to be implemented in a fashion that allows the circut-interface to obtain the digital signal.