# ITT3_solar_panel_group1


### Group members:
[Jonas]        
[Hrvoje]    



### Documentation: 
:pencil:
[documentation]


### Code
:computer:
[code]


### Tools:
<img align="left" alt="Git" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png" />
<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png" />
<img align="left" alt="Python" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" />
<img align="left" alt="Linux" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/linux/linux.png" />
<img align="left" alt="Raspberrypi" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/raspberry-pi/raspberry-pi.png" />



[pages]:tbd
[documentation]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group1/-/tree/main/documentation
[code]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group1/-/tree/main/code
[Jonas]: https://gitlab.com/Jonasbinti
[Hrvoje]: https://gitlab.com/cleksi





